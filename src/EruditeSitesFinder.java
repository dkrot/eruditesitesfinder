import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.StringTokenizer;


public class EruditeSitesFinder extends Configured implements Tool {
    public static class EruditeMapper extends Mapper<LongWritable, Text, TextPair, IntWritable> {
        static private IntWritable one = new IntWritable(1);

        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String line = value.toString();
            String[] items = line.split("\t");

            if (items.length != 2) {
                context.getCounter("COMMON_COUNTERS", "MalformedRecords").increment(1);
                return;
            }

            String host;
            try {
                URL url = new URL(items[0]);
                host = url.getHost();
            } catch (MalformedURLException e) {
                context.getCounter("COMMON_COUNTERS", "MalformedUrls").increment(1);
                return;
            }

            StringTokenizer tok = new StringTokenizer(items[1]);
            while (tok.hasMoreTokens()) {
                context.write(new TextPair(host, tok.nextToken()), one);
            }
        }
    }

    // партиционируем только по хосту - чтобы все запросы к одному хосту пошли на один reducer
    public static class OnlyHostPartitioner extends Partitioner<TextPair, IntWritable> {
        @Override
        public int getPartition(TextPair textPair, IntWritable num, int numPartitions) {
            return Math.abs(textPair.getFirst().hashCode()) % numPartitions;
        }
    }

    public static class OnlyHostComparator extends WritableComparator {
        protected OnlyHostComparator() {
            super(TextPair.class, true);
        }

        @Override
        public int compare(WritableComparable a, WritableComparable b) {
            Text host_a = ((TextPair)a).getFirst();
            Text host_b = ((TextPair)b).getFirst();
            return host_a.compareTo(host_b);
        }
    }

    // сравниваем по полному ключу чтобы упорядочить и по хостам, и по словам на reducer-е
    public static class FullKeyComparator extends WritableComparator {
        protected FullKeyComparator() {
            super(TextPair.class, true);
        }

        @Override
        public int compare(WritableComparable a, WritableComparable b) {
            return ((TextPair)a).compareTo((TextPair)b);
        }
    }

    public static class EruditeReducer extends Reducer<TextPair, IntWritable, Text, IntWritable> {
        @Override
        protected void reduce(TextPair key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            Text host = new Text(key.getFirst());
            Text prev_word = new Text();
            int  n_uniq = 0;

            for (IntWritable i: values) {
                if (prev_word.compareTo(key.getSecond()) != 0) {
                    n_uniq++;
                    prev_word.set(key.getSecond());
                }
            }

            context.write(host, new IntWritable(n_uniq));
        }
    }

    @Override
    public int run(String[] args) throws Exception {
        Job job = GetJobConf(getConf(), args[0], args[1]);
        return job.waitForCompletion(true) ? 0 : 1;
    }

    public static Job GetJobConf(Configuration conf, String input, String out_dir) throws IOException {
        Job job = Job.getInstance(conf);
        job.setJarByClass(EruditeSitesFinder.class);
        job.setJobName(EruditeSitesFinder.class.getCanonicalName());

        FileInputFormat.addInputPath(job, new Path(input));
        FileOutputFormat.setOutputPath(job, new Path(out_dir));

        job.setMapperClass(EruditeMapper.class);
        job.setReducerClass(EruditeReducer.class);

        job.setPartitionerClass(OnlyHostPartitioner.class);
        job.setSortComparatorClass(FullKeyComparator.class);
        job.setGroupingComparatorClass(OnlyHostComparator.class);

        // наш mapper пишет композитные ключи ключи(host:query) без значений
        job.setMapOutputKeyClass(TextPair.class);
        job.setMapOutputValueClass(IntWritable.class);

        // reducer выводит хост и лучший запрос с кол-вом повтором (текстом)
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        return job;
    }

    public static void main(String[] args) throws Exception {
        int rc = ToolRunner.run(new EruditeSitesFinder(), args);
        System.exit(rc);
    }
}
